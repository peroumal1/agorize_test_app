# README

In this repository is my very first Ruby application I wrote for the second part of the test!

# Environment

To run this application I used:

* MacOS X Mojave
* ruby 2.6.3p62 (set via rbenv)
* rails 5.2.3 
* Postgres 11

I only launched the application using the dev server provided with rails.

# Things that work

Although the final application is super ugly:

* new users can be created
* new skills can be created
* the query result from the first exercise is displayed on the skills index
* some very basic tests were added:
    * basic checks for skills and users
    * system check for user page

# Where did I ~~lost~~ spent most of my time doing this exercise (aka my limitations)

I was basically discovering Ruby and Rails (and Postgres), so it took me a while
to figure out how to set up my environment correctly. I used first the Ruby version
provided with my operating system, just to see that it would be a pain to get it to
work with my Postgres installation (library linking). The workaround was to install
Ruby and then Rails using rbenv, which got me with a working setup in a breeze.

Then I started up with Rails. I did like all the facilities the framework provide to
create models and controllers: I know it inspired lots of frameworks, so the concepts
looked pretty familiar and I was able to get the User model and controller basics working
pretty quickly (bonus point for the seeding functionality that allowed me to have real
data - borrowed from the first question - while doing the development).

The other two models were a bit more complicated for me. I was not sure the framework
would allow me to create automatically this kind of complicated models (self-referencing and associations)
so I went with trying to create it manually, only to notice later on (read: when I was stuck)
that Rails could have handled most of that boilerplate for me. Added, I also noticed
later on that I could have done better the self-referencing table, which I tried to correct
without success. 

Time still flying, I went for the pragmatic way and made some tradeoffs:

*  I gave up on adding a way to associate a skill to one or more users (if you have a working example, I would be glad to see it)
*  as for displaying the result from the query of the first question, I went for executing the raw SQL to get the result (is that cheating?)
*  I could have done better and factorized the views
*  I added some tests but they are minimal (by the way, I found fixtures really impratical)

And here we are, with the current version of the application I pushed.

# Final thoughts

Doing this exercise was really fun, and Rails is all things considered a great framework to start with. I am really eager
to at least learn more about the way you are using it and your practice.


