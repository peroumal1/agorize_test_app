class Skill < ApplicationRecord
  has_one :skill
  has_many :skill_users
  has_many :users, through: :skill_users
  validates :skill_id, numericality: {only_integer: true, allow_nil: true}
end
