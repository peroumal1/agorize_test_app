class User < ApplicationRecord
    has_many :skill_users
    has_many :skills, through: :skill_users
    validates :points, numericality: { only_integer: true}
end
