class SkillsController < ApplicationController
    def index
        @skills = Skill.all 
        @score = Skill.connection.select_all("
with recursive skills_score(id,name,points,users) as (
    select s.id, s.name, u.points, u.id from skills s, users u, skill_users su
    where s.skill_id is null and u.id = su.user_id and s.id = su.skill_id
    union 
    select skills_score.id, skills_score.name, u2.points, u2.id from skills s2, users u2, skill_users su2, skills_score
    where s2.skill_id = skills_score.id and u2.id = su2.user_id and s2.id = su2.skill_id
) select id,name,sum(points) as points,count(users) as users_count from skills_score group by id,name order by id") 
    end

    def show
        @skill = Skill.find(params[:id])
    end

    def new
        @skill = Skill.new
    end

    def create
        @skill = Skill.new(skill_params)
        if @skill.save
            redirect_to @skill
        else
            render 'new'
        end
    end

    def score_query
    end


    private
        def skill_params
            params.require(:skill).permit(:name,:skill_id)
        end
end
