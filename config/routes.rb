Rails.application.routes.draw do
  get 'welcome/index'
  resources :users
  resources :skills
  root 'welcome#index'
end
