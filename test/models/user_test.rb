require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "User can be saved with points" do
    user = User.new(points: 123)
    assert user.save
  end

  test "User cannot be saved without points" do
      user = User.new
      assert_not user.save
  end

  test "Points are expected to be int" do
    user = User.new(points: "abc")
    assert_not user.save
  end
end
