require 'test_helper'

class SkillTest < ActiveSupport::TestCase
   test "skill can be created" do
      skill = Skill.new(name:"test_skill",skill_id: nil) 
      assert skill.save
   end

   test "skill can be associated to a parent skill" do
      skill = Skill.new(name: "test_skill2",skill_id: 1)
      assert skill.save
   end 

end
