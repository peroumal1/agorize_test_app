# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

users = [
    [1,100],
    [2,200],
    [3,100],
    [4,50],
    [5,10]
]

users.each do |id,user_points|
    User.create!(id: id,points: user_points)
end

skills = [
    [1,"Football",nil],
    [2,"Basketball",nil],
    [3,"Foot",1],
    [4,"Basket",2],
    [5,"Soccer",1]
]

skills.each do |id,name,parent|
    Skill.create!(id: id,name: name,skill_id: parent)
end

skills_and_users = [
    [1,1,1],
    [2,1,2],
    [3,3,3],
    [4,2,4],
    [5,5,5]
]

skills_and_users.each do |id,skill_id,user_id|
    SkillUser.create!(id: id,skill_id: skill_id,user_id: user_id)
end

# maintain the id seq in sync so that whenever we insert a new entry it
# picks the correct id seq for pk

ActiveRecord::Base.connection.tables.each do |t|
      ActiveRecord::Base.connection.reset_pk_sequence!(t)
end
